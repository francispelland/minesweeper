(function($) {
	"use strict";

	// Global Variables/Selectors (Common to all)
	var $masonryGrid = $("#masonry-grids"),
		$featuredPost = $(".featured-post"),
		$pageContents = $(".page-contents"),
		supportTransitions = Modernizr.csstransitions;

	/** Masonry Grids Config **/
	function masonryConfig() {
		var $item = $("> li.grid-item", $masonryGrid);

		function masonryInit() {
			$masonryGrid.masonry({
				itemSelector: "li.grid-item"
			});
		}
		// Re-order Layout
		function reArrange() {
			function compare(a, b) {
				return ($(a).data("cell") > $(b).data("cell")) ? 1 : -1;
			}
			$item.sort(compare).prependTo($masonryGrid.selector);
		}

		// Init Events & functions
		masonryInit();	// First Time Initialization
		$(window).on("load resize", function(e) {
			var width = $(this).width()
			reArrange();	// call reArrange Function (on every resize)
			

			$masonryGrid.masonry("destroy");	// destroy previous initialization
			masonryInit();	// Reinitialize
		});

	}

	/** Parallax Config **/
	function parallaxEffect() {
		$pageContents.on("scroll", function(e) {
			var offset = $(this).scrollTop(),
				by = 4;	// edit this
			$(".parallax").each(function() {
				$(this).css("background-position", "center " + (-offset/by) + "px");
			});
			e.stopPropagation();
		});
	}

	/** Initialization of all Functions Here **/
	function init() {
		masonryConfig();
		parallaxEffect();
		
		var minesweeper = new Minesweeper();
        minesweeper.init($('.minesweeper'));
	};
	init();

})(jQuery);