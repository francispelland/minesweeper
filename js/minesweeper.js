var Minesweeper;

'use strict';

// standard level configurations
var levels = {
    'easy': {
        'boardSize': [9, 9],
        'numMines': 10
    },
    'medium': {
        'boardSize': [16, 16],
        'numMines': 40
    },
    'hard': {
        'boardSize': [30, 16],
        'numMines': 99
    }
};

Minesweeper = function () {
    var msObj = this;
    this.options = {selectedLevel : "easy"};
    this.over = false;
    this.running = false;
    this.timer = '';
    this.isFirstMove = 1;
    this.totalFlagged = 0;
        
    var Cell = function Cell(x, y, isMine) {
        this.x = x;
        this.y = y;
        this.isMine = isMine;
        this.hit = 0;
        
        this.activeNeighbours = 0;
        this.neighbours = [];
        this.revealed = false;
        this._mark = "";
        this.id = y + "-" + x;
    };
    
    Cell.prototype = {
    	setValue : function(value){
    		var el = $("#game").find("li[data-index='"+this.id+"']");
	        el.removeClass('flag').html("");

	        if (parseInt(value, 10) > 0) el.attr("class", "tile_"+value)

	        switch(value) {
	            case "!":
	            	el.attr("class","flag");
	            	break;
	            case "?":
	                el.html("?");
	                break;
	            case "X":
	            	if (this.hit == 1)
	            		el.attr("class","mine_hit");
	            	else
	            		el.attr("class","mine");
	            	
	            	if (this._mark == "!"){
		                el.attr("class","mine_marked");
	            	}
	            	
	                break;
	            default:
	            	if (this.revealed)
	            		el.addClass('revealed');
	        }
	        

	        return this;
    	},
    	
    	moveMine : function(cell, avoidX, avoidY){
    		var selectedLevel = msObj.options.selectedLevel;
			var colls = levels[selectedLevel].boardSize[0],
				rows = levels[selectedLevel].boardSize[1];

    		var startX = (avoidX - 1) >= 0 ? (avoidX - 1) : 0;
    		var endX = (avoidX + 1) <= colls ? (avoidX + 1) : colls;
    		var startY = (avoidY - 1) >= 0 ? (avoidY - 1) : 0;
    		var endY = (avoidY + 1) <= rows ? (avoidY + 1) : rows;
    		
    		var possible = [];
    		for(var i = 0; i<rows; i++ ) {
                for(var j=0; j<colls; j++) {
                    if (i>=startY && i<=endY && j>=startX && j<=endX ) continue;
                    
                    possible.push([i, j]);
                }
            }
    		
    		var available = $(possible).not(Board.placedMines).get();
    		var newLocation = available[Math.floor(Math.random()*available.length)];

    		cell.isMine = 0;    		
    		var newCell = Board.getCell(newLocation[1], newLocation[0]);
    		if (newCell == undefined) console.error(newLocation)
    		newCell.isMine = 1;
    		Board.refreshNeighbours();
    	},
    	
    	addNeighbour : function(neighbour){
            this.neighbours.push(neighbour);
            this.activeNeighbours += (neighbour.isMine ? 1 : 0);
            return this;
        },
        
        getNeighbours : function(neighbour){
            return this.neighbours;
        },
        
        isRevealed : function(neighbour){
            return this.revealed || this.isMine;
        },
        
        reveal : function(fn, context){
        	if (msObj.isFirstMove == 1){
        		if (this.isMine){
        			var thisCell = this;
        			this.moveMine(thisCell, thisCell.x, thisCell.y);
        			this.isMine = 0;
        		} 
        		
        		if (this.activeNeighbours > 0){
        			var thisCell = this;
        			$.each(this.getNeighbours(), function(i, neighbour){
                        if(neighbour.isMine) {
                        	this.moveMine(neighbour, thisCell.x, thisCell.y)
                        }
                    });
        			this.activeNeighbours = 0;
        		}
        	}
        	msObj.isFirstMove = 0;
        	
            if(this.isMine) {
            	this.hit = 1;
                return;
            }
            
            if (this._mark == "!"){
            	return;
            }
            
            this.revealed = true;
            this.setValue(this.activeNeighbours);
            if(this.activeNeighbours === 0) {
                $.each(this.getNeighbours(), function(i, neighbour){
                    if(!neighbour.isRevealed()) {
                        neighbour.reveal(fn, context);
                    }
                });
            };
            fn.call(context);
        },
        
        mark : function() {
            if(this.revealed) {
                return ;
            }
            switch(this._mark) {
                case "":
                	msObj.totalFlagged++;
                    this._mark = "!";
                    break;
                case "!":
                	msObj.totalFlagged--;
                    this._mark = "?";
                    break;
                case "?":
                    this._mark = "";
                    break;
            }
            this.setValue(this._mark || "&nbsp;");
            return this;
        }
    };
    
    var Board = {
    	placedMines : [],
		reset: function() {
			var selectedLevel = msObj.options.selectedLevel;
			var colls = levels[selectedLevel].boardSize[0],
				rows = levels[selectedLevel].boardSize[1];
			
            var x, y, z = 0, row, self = this;
            
            this.colls = colls;
            this.rows = rows;
            
            this.board = [];   
            this.placedMines = [];
            var mines = this.generateMineArray();
            
            // build the data structure
            for(y=0; y<this.rows; y++) {
                row = [];
                this.board.push(row);
                for(x=0; x<this.colls; x++) {
                    var isMine = mines[z++];
                    if (isMine) 
                    	this.placedMines.push([x, y])
                    	
                    row.push(new Cell(x, y, isMine));
                }
            }
            
            this.refreshNeighbours();
            
            return this;
        },
        
        refreshNeighbours : function(){
        	var self = this
        	this.traverseCells(function(cell) {
        		cell.neighbours = [];
        		cell.activeNeighbours = 0;
                var neighbours = self.calcNeighbours(cell);
                $.each(neighbours, function(key, value){
                    cell.addNeighbour(value);
                });
            });
        },
    	
        traverseCells: function(fn) {
            var x, y;
            for(y=0; y<this.rows; y++){
                for(x=0; x<this.colls; x++){
                    fn(this.getCell(x, y));
                }
            }
        },
        
        draw: function(){
            var x, y, ul, li;
            $("#game").empty();
            
            for(y=0; y<this.rows; y++) {
                ul = $('<ul class="list-inline">');
                for(x=0; x<this.colls; x++) {
                    li = $('<li data-index="'+y+'-'+x+'">').html("&nbsp;");
                    ul.append(li);
                }
                $("#game").append(ul);
            }
            var width = $("#game ul li").outerWidth();
            $("#game").css('width', (width * this.colls));
            return this;
        },
        
        validate: function(){
            var numActive = 0;
            this.traverseCells(function(cell) {
                numActive += cell.isRevealed() ? 0 : 1;
            });
            
            if(!numActive) {
                this.checkMines();
                msObj.endGame(true);
            }
        },
        
        reveal: function(cell, passive){
            cell.reveal(this.validate, this);
            if(cell.isMine) {
                this.checkMines();
                msObj.endGame(false);
            }
        },
        
        getCell: function(x,y) {
            return this.board[y][x];
        },
        
        generateMineArray : function () {
        	var selectedLevel = msObj.options.selectedLevel;
            var width = levels[selectedLevel].boardSize[0],
                height = levels[selectedLevel].boardSize[1],        
                totalMines = levels[selectedLevel].numMines;
            
            function shuffle(array) {
    			var currentIndex = array.length, temporaryValue, randomIndex;
    		
    			while (0 !== currentIndex) {
    				randomIndex = Math.floor(Math.random() * currentIndex);
    				currentIndex -= 1;
    		
    				temporaryValue = array[currentIndex];
    				array[currentIndex] = array[randomIndex];
    				array[randomIndex] = temporaryValue;
    			}
    		
    			return array;
            }
            
            var appliedMines = 0;        
            var mines = Array.apply(null, {length: width * height}).map(function(x, i){ 
            	appliedMines++;
            	return appliedMines <= totalMines ? 1 : 0; 
            });
            
            return shuffle(mines);
        },
        
        checkMines: function(){
            this.traverseCells(function(cell) {
                if(cell.isMine) {
                    cell.setValue("X");
                }
            });
        },
        
        getCellByEvent: function(e) {
        	var theEl = $(e.target);
        	
        	if (theEl.is("i"))
        		theEl = theEl.parent();
        	
            var cords = theEl.attr('data-index').split('-');
            var y = +cords[0], x=+cords[1];
            return this.getCell(x, y);
        },
        
        calcNeighbours: function(cell) {
            var i, j, data = [], x = cell.x, y = cell.y;
            for( i=y-1; i<=y+1; i++ ) {
                if(i<0 || i>=this.rows)  continue;
                for(j=x-1; j<=x+1; j++) {
                    if(j<0 || j>=this.colls) continue;
                    if(x===j && y===i) continue;
                    data.push(this.getCell(j,i));
                }
            }
            return data;
        }
    };
    

    this.init = function (gameEl, options) {
        msObj.options = $.extend({}, msObj.options, options || {});
        msObj.resetGame();
        
        msObj.initHandlers(gameEl);
        return msObj;
    };

    this.initHandlers = function (msEl) {    	
    	msEl.on('click', '.face', this.handleFaceClick);
    	msEl.on('mousedown', '.face', function(e) {
    	    $(this).find('img').attr('src', 'images/face_down.png')
    	});
    	msEl.on('mouseleave mouseup', '.face', function(e) {
    	    $(this).find('img').attr('src', 'images/face_up.png') 
    	});
    	
    	msEl.on('change', '.game_difficulty', function(e){
    		msObj.changeLevel($(this).val());
    	});
    };
    
    this.handleCellClick = function(event){
         if (event.button === 0) {
        	 if (msObj.running === false){
        		 msObj.startGame();
         	}
         	
         	if(!this.over) {
                 Board.reveal(Board.getCellByEvent(event));
             }
        } else if (event.button === 2) {
        	event.preventDefault();
            if(!this.over) {
                Board.getCellByEvent(event).mark();
                
                var selectedLevel = msObj.options.selectedLevel;
                $('#game_mines').text(pad(levels[selectedLevel].numMines - msObj.totalFlagged, 3))
            }
        }
    }
    
    this.handleCellMouseDown = function(event){
    	setFace('face_limbo');
    	
    	if (event.button === 1) {
        	var cell = Board.getCellByEvent(event);
        	if (!cell.revealed && !cell.flagged){
        		cell.view = 1;
         	   
         	   var el = $("#game").find("li[data-index='"+cell.id+"']");
         	   el.addClass('revealed');
            }        	
        	
        	$.each(cell.getNeighbours(), function(i, neighbour){
               if (!neighbour.revealed && !neighbour.flagged){
            	   neighbour.view = 1;
            	   
            	   var el = $("#game").find("li[data-index='"+this.id+"']");
            	   el.addClass('revealed');
               }
            });
        } 
    }
    
    this.handleCellMouseOut = function(event){
    	setFace('face_up');
    	
    	if (event.button === 1) {
    		Board.traverseCells(function(cell){
    			if (cell.view == 1){
             		delete cell.view;
             	   
             		var el = $("#game").find("li[data-index='"+cell.id+"']");
             		el.removeClass('revealed');
    			}
	   		});
        } 
    }

    this.handleFaceClick = function(event){
    	if (msObj.running === true || msObj.over === true){
    		msObj.resetGame();
    	}
    };

    this.setBoardOptions = function () {
        var level = this.level;
    };

    this.startGame = function () {
    	msObj.running = true;
        var timerElement = $('#game_timer');
        timerElement.text(pad(0,3));

        msObj.timer = window.setInterval(function () {
            var curr = parseInt(timerElement.text(), 10);
            timerElement.text(pad(curr + 1, 3));
        }, 1000);
    };
    
    this.endGame = function(win){	
    	if (this.over === true) return;
    	
    	this.over = true;
    	this.running = false;
    	this.stopTimer();
    	$('.minesweeper #game li').off();
    	
    	var selectedLevel = msObj.options.selectedLevel;
    	var totalTime = parseInt($('#game_timer').text(), 10);;
    	
    	if (win === true){
    		setFace('face_win');
    		
    		var challengeMsg = "";
    		if (selectedLevel == "easy")
    			challengeMsg = "Perhaps try the more challenging medium or hard skill levels?";
    		else if (selectedLevel == "medium")
    			challengeMsg = "You may very well be an expert. Go ahead, try it on hard!";
    		
    		$.smallBox({
				title : "You Rock!",
				content : "You've successfully beat Minesweeper on "+selectedLevel+" in "+totalTime+" seconds! "+challengeMsg,
				color : "#5b835b",
				timeout: 8000,
				icon : "fa fa-trophy"
			});
    	} else {
    		setFace('face_lose');
    		
    		var challengeMsg = "";
    		if (selectedLevel == "medium")
    			challengeMsg = "The easy level will give you a chance to work on your skills!";
    		else if (selectedLevel == "hard")
    			challengeMsg = "Perhaps you aren't quite the expert yet. Try a lower skill level?";
    		
    		$.smallBox({
				title : "Oh no!",
				content : "You've found a mine. Try again. "+challengeMsg,
				color : "#77021d",
				timeout: 8000,
				icon : "fa fa-frown-o"
			});
    	}
    };

    this.stopTimer = function () {
        if (msObj.timer) {
            window.clearInterval(msObj.timer);
        }
    };

    this.resetGame = function () {
    	msObj.stopTimer();
    	
        var level = 'easy';
        var numMines = levels[level].numMines;

        $('#game_mines').text(pad(numMines, 3));
        $('#game_timer').text(pad(0, 3));
        setFace('face_up');
        
        msObj.running = false;
        msObj.over = false;
        msObj.totalFlagged = 0;
        msObj.isFirstMove = 1;
        
        Board.reset().draw();
        
        $('.minesweeper #game li').on('click auxclick', msObj.handleCellClick);
        $('.minesweeper #game li').on('contextmenu', function(e){ e.preventDefault()});
        $('.minesweeper #game li').on('mouseleave mouseup', msObj.handleCellMouseOut);
        $('.minesweeper #game li').on('mousedown', msObj.handleCellMouseDown);
    };
    
    this.changeLevel = function(level){
    	msObj.options.selectedLevel = level;
        msObj.resetGame();
    };

    this.checkBestTime = function (time) {
    	var selectedLevel = msObj.options.selectedLevel;
        var bestTime = localStorage.getItem('best_time_' + selectedLevel);

        if (!bestTime || parseInt(time, 10) < parseInt(bestTime, 10)) {
            var displayName = localStorage.getItem(selectedLevel + '_record_holder');
            if (!displayName) {
                displayName = 'Your name';
            }
            var name = window.prompt(
                'Congrats! You beat the best ' + level + ' time!', displayName
            );

            localStorage.setItem('best_time_' + selectedLevel, time);
            localStorage.setItem(selectedLevel + '_record_holder', name);
        }        
    };
    
    function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
    
    function setFace(face){
    	$('.minesweeper .face img').attr('src', 'images/'+face+'.png') 
    }
};	


$('#best_times').on('click', function () {
    var easyTime = localStorage.getItem('best_time_easy') || 'None';
    var mediumTime = localStorage.getItem('best_time_medium') || 'None';
    var hardTime = localStorage.getItem('best_time_hard') || 'None';
    
    var easyName = localStorage.getItem('easy_record_holder') || 'None';
    var mediumName = localStorage.getItem('medium_record_holder') || 'None';
    var hardName = localStorage.getItem('hard_record_holder') || 'None';
    
    alert('Best times:\nEasy:\t' + easyName + '\t' + easyTime + '\n' +
        'Medium:\t' + mediumName + '\t' + mediumTime + '\n' +
        'Hard:\t' + hardName + '\t' + hardTime);
});